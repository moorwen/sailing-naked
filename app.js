var http = require('http'),
  fs = require('fs'),
  httpProxy = require('http-proxy');

httpProxy.createServer({
  target: {
    host: '108.62.5.30',
    port: 443
  },
  ssl: {
    key: fs.readFileSync('key.pem', 'utf8'),
    cert: fs.readFileSync('cert.pem', 'utf8'),
    passphrase: 'gundamseed'
  }
}).listen(443);